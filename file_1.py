# Завдання 1
#
# Створіть функцію, яка відображає привітання для користувача із заданим
# ім'ям. Якщо ім'я не вказано, вона повинна виводити привітання для користувача з Вашим ім'ям.

def welcome_user(name: str = "Mykhailo"):
    """The function displays a greeting for the user with the given name"""
    print(f"Hello, {name}!")


# A function call without parameters
welcome_user()

# Call function with username
name = input("Enter your name: ")
welcome_user(name)
