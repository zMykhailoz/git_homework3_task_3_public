# Завдання 3
#
# Створіть програму-калькулятор, яка підтримує наступні операції: додавання, віднімання, множення, ділення,
# зведення в ступінь, зведення до квадратного та кубічного коренів. Всі дані повинні вводитися в циклі,
# доки користувач не вкаже, що хоче завершити виконання програми.
# Кожна операція має бути реалізована у вигляді окремої функції.
# Функція ділення повинна перевіряти дані на коректність та видавати повідомлення про помилку у разі спроби поділу
# на нуль.
import math

def addition(num_1, num_2):
    """Function to add"""
    return num_1 + num_2

def subtraction(num_1, num_2):
    """Subtraction function"""
    return num_1 - num_2

def multiplication(num_1, num_2):
    """Function for multiplication"""
    return num_1 * num_2

def division(num_1, num_2):
    """Function for division"""
    if num_2 == 0:
        print("Division by zero is not possible")
    return num_1 / num_2

def exponentiation(num_1, num_2):
    """Function for exponentiation"""
    return num_1 ** num_2

def reduction_to_square(number):
    """Square root function"""
    return math.sqrt(number)

def reduction_to_cube_roots(number):
    """Cube root function"""
    return number ** (1/3)

# The main cycle
while True:
    # Selection menu
    print("\nChoose operation:")
    print("1. Addition")
    print("2. Subtraction")
    print("3. Multiplication")
    print("4. Division")
    print("5. Exponentiation")
    print("6. Square Root")
    print("7. Cubic Root")
    print("8. Exit")

    choice = input("Enter choice (1-8): ")

    # Exit from the cycle and end
    if choice == "8":
        print("Exiting the calculator. Goodbye!")
        break

    # Enter one number for item 6 and 7, else enter two numbers
    if choice == "6" or choice == "7":
        number = float(input("Enter a number: "))
    elif choice in ["1", "2", "3", "4", "5"]:
        num_1 = float(input("Enter first number: "))
        num_2 = float(input("Enter second number: "))

    # result
    if choice == "1":
        print(f"{num_1} + {num_2} = {addition(num_1, num_2)}")
    elif choice == "2":
        print(f"{num_1} - {num_2} = {subtraction(num_1, num_2)}")
    elif choice == "3":
        print(f"{num_1} * {num_2} = {multiplication(num_1, num_2)}")
    elif choice == "4":
        if num_2 == 0:
            print("Division by zero is not allowed.")
        else:
            print(f"{num_1} / {num_2} = {division(num_1, num_2)}")
    elif choice == "5":
        print(f"{num_1} ** {num_2} = {exponentiation(num_1, num_2)}")
    elif choice == "6":
        print(f"Square root of {number} = {reduction_to_square(number)}")
    elif choice == "7":
        print(f"Cubic root of {number} = {reduction_to_cube_roots(number)}")

    else:
        print("Invalid choice. Please enter a number from 1 to 8.")


